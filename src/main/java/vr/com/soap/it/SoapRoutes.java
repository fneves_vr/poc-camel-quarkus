package vr.com.soap.it;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.builder.endpoint.EndpointRouteBuilder;
import org.apache.camel.dataformat.soap.SoapJaxbDataFormat;
import org.apache.camel.dataformat.soap.name.TypeNameStrategy;

import vr.com.soap.it.example.GetCustomersByName;

@ApplicationScoped
public class SoapRoutes extends EndpointRouteBuilder  {

    @Override
    public void configure() {
        final String soapPackage = GetCustomersByName.class.getPackage().getName();
        SoapJaxbDataFormat soap = new SoapJaxbDataFormat(soapPackage, new TypeNameStrategy());

        from("direct:marshal")
                .marshal(soap);

        from("direct:unmarshal")
                .unmarshal(soap);

        from("direct:marshal-soap12")
                .marshal().soapjaxb12(soapPackage);

        from("direct:unmarshal-soap12")
                .unmarshal().soapjaxb12(soapPackage);
    }
}
