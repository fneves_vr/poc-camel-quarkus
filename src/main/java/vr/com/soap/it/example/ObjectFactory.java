package vr.com.soap.it.example;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.example.customerservice.multipart package.
 * <p>
 * An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups. Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCustomersByName_QNAME = new QName(
            "http://example.it.soap.component.quarkus.camel.apache.org/",
            "getCustomersByName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * com.example.customerservice
     * .multipart
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCustomersByName }
     */
    public GetCustomersByName createGetCustomersByName() {
        return new GetCustomersByName();
    }

    @XmlElementDecl(namespace = "http://example.it.soap.component.quarkus.camel.apache.org/", name = "getCustomersByName")
    public JAXBElement<GetCustomersByName> createGetCustomersByName(GetCustomersByName value) {
        return new JAXBElement<GetCustomersByName>(_GetCustomersByName_QNAME, GetCustomersByName.class, null, value);
    }
}
